//version inicial

const express = require('express'),
_         = require('lodash'),
config    = require('./config'),
cors      = require('cors'),
jwt       = require('jsonwebtoken'),
session   = require('express-session'),
base64url = require('base64url'),
bcrypt 		= require('bcryptjs'),
app       = express(),
port      = process.env.PORT || 3000;

//let app = module.exports = express.Router();

let path          = require('path');
let bodyparser    = require('body-parser');
let requestjson   = require('request-json');

let urlCustomerMLab = "https://api.mlab.com/api/1/databases/lgarcia/collections/Users?apiKey=GOLqWa850qO8tsdCUdby6eq9eKPInBkt";
let urlMLabRoot = "https://api.mlab.com/api/1/databases/lgarcia/collections/";
let apiKey ="apiKey=GOLqWa850qO8tsdCUdby6eq9eKPInBkt";
let userMLabRoot;

let customerMLab = requestjson.createClient(urlCustomerMLab);

app.use(session({
	secret: 'secret',
	resave: true,
	saveUninitialized: true
}));

app.use(bodyparser.json());
app.listen(port);
app.use(cors());

app.use(function (req, res, next) {
	res.header("Access-Control-Allow-Origin", "*");
	res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
	next();
});

console.log('RESTful API server started on: ' + port);

//base URL, function
app.get('/', cors(), function (req, res){

	res.sendFile(path.join(__dirname,'index.html'));

});

function createIdToken(usernameSession) {
	return jwt.sign(_.omit(usernameSession, 'password'),
	Buffer.from(config.secret).toString('base64'),
	{ expiresIn: 60*60*5 });
}

function createAccessToken() {
	return jwt.sign({
		iss: config.issuer,
		aud: config.audience,
		exp: Math.floor(Date.now() / 1000) + (60 * 60),
		scope: 'full_access',
		//sub: "lalaland|gonto",
		jti: genJti(), // unique identifier for the token
		alg: 'HS256'
	}, Buffer.from(config.secret).toString('base64'));
}

// Generate Unique Identifier for the access token
function genJti() {
	let jti = '';
	let possible = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
	for (let i = 0; i < 16; i++) {
		jti += possible.charAt(Math.floor(Math.random() * possible.length));
	}

	return jti;
}

app.post('/v1/add/Users', cors(), function(req, res) {

	if(typeof req.body.name == "undefined" ||
	typeof req.body.lastname == "undefined" ||
	typeof req.body.email == "undefined" ||
	typeof req.body.password == "undefined" ||
	typeof req.body.passwordConfirm == "undefined") {

		res.status(400).send({ error : "Missing data in fields" });

	}else{

		if(req.body.password === req.body.passwordConfirm){

			const BCRYPT_SALT_ROUNDS = 12;

			bcrypt.hash(req.body.password, BCRYPT_SALT_ROUNDS)
			.then(function(hashedPassword) {

				let body = {
					email 		: req.body.email,
					name 			: req.body.name,
					lastname 	: req.body.lastname,
					profile 	: req.body.profile,
					status 		: 1,
					password 	: hashedPassword
				};

				customerMLab.post('', body, function(err, resM, body){

					res.status(201).send(body);

				});

			})
			.catch(function(error){

				console.log({ error :"Error saving user: "});
				console.log(error);
				next();

			});

		}else{

			res.status(401).send({ error : "Your password and confirmation password do not match." });

		}

	}

});


app.post('/v1/Sessions', cors(), function(req, res) {

	let email = req.body.email;
	let password = req.body.password;
	let isUserSession = req.session.loggedin;
	let usernameSession = req.session.username;

	if (!req.body.email || !req.body.password) {

		res.status(400).send({ error : "You must send the username and the password" });

	} else {

		//const query = 'q={"email":"' + email + '","password":"' + password + '"}';
		const query = 'q={"email":"' + email + '"}';

		userMLabRoot = requestjson.createClient(urlMLabRoot + "Users?" + apiKey + "&" + query);

		userMLabRoot.get('',function(error, resM, body){
			//console.log(body[0]);
			if (typeof body[0] == "undefined") {

				res.status(400).send({ error : "Unregistered user¡" });

			}else{

				bcrypt.compare(password, body[0].password, function(err, result) {

					if (err) { throw (err); }

					//console.log(result);
					//if (body.length == 0) {
					if (!result) {

						res.status(401).send({ error : "The username or password don't match" });

					} else {

						isUserSession = result;
						usernameSession = body[0].email;
						let name = body[0].name;
						let profile = body[0].profile;

						res.status(201).send({

							username : usernameSession,
							name 		 : name,
							profile	 : profile,
							session  : {
								isUserSession : isUserSession,
								idToken 			: createIdToken(usernameSession),
								accessToken 	: createAccessToken()
							}

						});

					}

				});
			}

		});

	}

});
